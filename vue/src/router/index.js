import Vue from 'vue'
import Router from 'vue-router'
import mainPage from '@/pages/p-main/page.vue'
import autocompletePage from '@/pages/p-autocomplete/page.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Главная',
            component: mainPage
        },
        {
            path: '/autocomplete',
            name: 'Автокомплит',
            component: autocompletePage
        }
    ]
})
